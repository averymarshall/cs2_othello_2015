To improve my AI, I spent most of my time attempting to optimize my 
minimax function. At the time I submitted Assignment 1, it had memory usage
bugs; I managed to iron these out by deleting memory as I iterated through
the tree (rather than merely at the end). I experimented with using a more
advanced heuristic function for the minimax, but found in practice that it 
performed better using the naive heuristic and iterating some plys deeper.
I then discovered a glitch in my competitive heuristic and, upon fixing it,
use my competitive heuristic successfully to compete at lower levels.
