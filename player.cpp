#include "player.h"
#include <string>

// Scores for different cases. 1 point represents the value of one piece.
//All scores stack (i.e. a corner will get the corner, diagonal, and edge
// scores factored in. An optimal configuration is 20, -30, 2, 5, -3.
const int CORNER_SCORE = 20;
const int CLOSE_CORNER_SCORE = -30; 
const int DIAGONAL_SCORE = 2;
const int EDGE_SCORE = 5;
const int CLOSE_EDGE_SCORE = -3;

// Corners
Move c1(0, 0);
Move c2(0, 7);
Move c3(7, 0);
Move c4(7, 7);

// Ply of minimax algorithm. Currently only works for 2-ply.
const int PLY = 1;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
   // Will be set to true in test_minimax.cpp.
   testingMinimax = true;

   /* 
    * TODO: Do any initialization you need to do here (setting up the board,
    * precalculating things, etc.) However, remember that you will only have
    * 30 seconds.
    */
    // Initializing set of legal moves, player side, and opponents side
	pSide = side;
    if(pSide == WHITE)
		oSide = BLACK;
	else
		oSide = WHITE;
		
}
/*
 * Sets the board state within the player. Takes a Board* as input.
 */
void Player::setBoard(Board* board)
{ b = *board; }
/*
 * Returns the score of a particular Move* as an int.
 */
int Player::hScore(Board* boa, Side side, Move* m)
{
	int x = m->getX();
	int y = m->getY();
	// Contains bonuses or minuses from a play close to or that is an
	// edge or corner.
	int bonus = 0;
	// Copy of the board state
	Board* board = boa->copy();
	// Adding a bonus for a corner play.
	if(board->sameMove(*m, c1) || board->sameMove(*m, c2) || board->sameMove(*m, c3)
	 || board->sameMove(*m, c4))
		bonus += CORNER_SCORE;		
	// Punishing a play that is adjacent to a corner.
	else if(board->nextTo(*m, c1) || board->nextTo(*m, c2) || board->nextTo(*m, c3) ||
		board->nextTo(*m, c4))
		bonus += CLOSE_CORNER_SCORE;
	// Adding a bonus for an edge play
	if(x == 0 || x == 7 || y == 0 || y == 7)
		bonus += EDGE_SCORE;
	// Punishing a play that is adjacent to an edge
	else if(x == 1 || x == 6 || y == 1 || y == 6)
		bonus += CLOSE_EDGE_SCORE;
	// Adding a bonus for a diagonal play
	if(abs((float)x - 3.5) == abs((float)y - 3.5))
		bonus += DIAGONAL_SCORE;
	// Updating the board copy
	board->doMove(m, side);
	// Returning the final score
	if(side == WHITE)
		return board->countWhite() - board->countBlack() + bonus;
	return board->countBlack() - board->countWhite() + bonus;
}
/*
 * Naively calculates the score taking into account only the number of 
 * pieces.
 * 
 * Takes a side and a Move* as input and outputs the score as an integer.
 */
int Player::naive(Board* board, Side s, Move* m)
{
	// Copying the board and making a move
	Board* bCopy = board->copy();
	bCopy->doMove(m, s);
	// Returning the final score
	if(s == WHITE)
		return (float)bCopy->countWhite() / (float)bCopy->countBlack();
	return (float)bCopy->countBlack() / (float)bCopy->countWhite();
}
/*
 * Minimax algorithm. Takes a board object, side, and integer as input.
 * The integer represents the ply number.
 * Returns a Move* representing the best move to make.
 */
Move* Player::minimax(Board* board, Side side, int p)
{
	int minScore = 100;
	int maxScore = -100;
	int score;
	Side otherSide;
	std::vector<Move*> moves = board->getMoves(side);
	Move* mMin;
	Move* mMax;
	// Getting the other side
	if(side == WHITE)
		otherSide = BLACK;
	else
		otherSide = WHITE;	
	// Iterating through all the legal moves of a given side
	for(unsigned int i = 0; i < moves.size(); i++)
	{
		if(p == 1) // Base case
			// Getting the score of the current legal move
			score = hScore(board, pSide, moves[i]);
		else // The recursion is still inside of the tree
		{
			// Getting the score of the rest of the tree
			// Generating new board
			Board* bCopy = board->copy();
			// Making a move on new board
			bCopy->doMove(moves[i], side);
			// Calculating score relative to the player	
			score = hScore(bCopy, pSide, minimax(bCopy, otherSide, p - 1));
		}
		// Determining the max and min scores
		if(score < minScore)
		{
			// Attempting to delete memory allocated while in the tree
			mMin = new Move(moves[i]->getX(), moves[i]->getY());			
			minScore = score;
		}
		if(score > maxScore)
		{
			// Attempting to delete memory allocated while in the tree
			mMax = new Move(moves[i]->getX(), moves[i]->getY());
			maxScore = score;
		}
		delete moves[i];
	}
	// Clearing moves vector before return
	moves.clear();
	// If this is the player, we maximize the output. If it is the opponent,
	// we minimize the output.
	if(side == pSide)
		return mMax;
	return mMin;
}
		
/*
 * Heuristic algorithm for determining the best move from the current
 * set of legal moves. Intended for more competitive play.
 * 
 * Returns a Move* representing the best move.
 */

Move* Player::competitive(Side side)
{
	std::vector<int> scores;
	int maxScoreIndex = -1;
	int maxScore = -1000;
	int s = 0;
	// Finding the largest score and its index
	for(unsigned int i = 0; i < legalMoves.size(); i++)
	{
		s = hScore(b.copy(), side, legalMoves[i]);
		if(s > maxScore)
		{
			maxScore = s;
			maxScoreIndex = i;
		}
	}
	return legalMoves[maxScoreIndex];
}

	
/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move* Player::doMove(Move* opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    // Deleting legal moves from previous run
	legalMoves.clear();
    Move* move;
    // Processing opponents move
    b.doMove(opponentsMove, oSide);
    // Passing if player has no moves
    if(!b.hasMoves(pSide))
		return NULL;
	// Determining next best move using competitive algorithm if not
	// testing minmax.
	if(!testingMinimax)
	{
		move = competitive(pSide);
		// Updating legal moves
		legalMoves = b.getMoves(pSide);
	}
	else
		move = minimax(b.copy(), pSide, PLY);
	// Updating board state
	b.doMove(move, pSide);	
	return move;
}
