#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
	int hScore(Board* boa, Side side, Move* m);
	int naive(Board* board, Side s, Move* m);
	Move* competitive(Side side);
	Move* minimax(Board* b, Side side, int p);
	void setBoard(Board* board);
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Board b;
    std::vector<Move*> legalMoves;
    Side pSide;
    Side oSide;
};

#endif
